package HW3;

import java.util.Scanner;

public class Hw8 {
    public static void main(String[] args){
        draw16();
    }

    static void draw16(){

        Scanner input = new Scanner(System.in);
        System.out.print("Input n: ");
        int n = input.nextInt();
        input.close();

        String a = "*";
        String b = "-";

        String str = "";

        for(int i = ((n*2)-1);i>0;i--){

            if(i>=n){
                for(int j = n; j>0;j--){
                    if((i-n)>=j){
                        str += b;
                    }
                    else{
                        str += a;
                    }
                }
            }else{
                for(int j = 1; j<=n;j++){
                    if((i-j)>=0){
                        str += a;
                    }
                    else{
                        str += b;
                    }
                }
            }

            str += "\n";

        }

        System.out.println(str);
    }
}
