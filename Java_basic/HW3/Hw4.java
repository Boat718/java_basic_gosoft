package HW3;

import java.util.Scanner;

public class Hw4 {
    public static void main(String[] args){
        draw12();
    }

    static void draw12(){

        Scanner input = new Scanner(System.in);
        System.out.print("Input n: ");
        int n = input.nextInt();
        input.close();

        String a = "*";
        String b = "-";

        String str = "";

        for(int i = 1;i<=n;i++){

            for(int j = 1; j<=n;j++){
                if((i-j) == 0){
                    str += b;
                }
                else{
                    str += a;
                }
            }
            str += "\n";

        }

        System.out.println(str);
    }
}
