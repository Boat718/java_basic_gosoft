package HW3;

import java.util.Scanner;

public class Hw1 {
    public static void main(String[] args){

        draw9();
    }

    static void draw9(){

        Scanner input = new Scanner(System.in);
        System.out.print("Input num: ");
        int n = input.nextInt();

        String str = "";

        for(int i=0;i<n;i++){
            int s = i*2;
            str+=s+"\n";
        }
        System.out.println(str);

        input.close();

    }
}
