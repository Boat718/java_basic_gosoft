package HW3;

import java.util.Scanner;

public class Hw3 {
    public static void main(String[] args){
        draw11();
    }

    static void draw11(){
        Scanner input = new Scanner(System.in);
        System.out.print("Input: ");
        int n = input.nextInt();
        input.close();

        String str = "";

        for(int i = 1;i<=n;i++){

            for(int j = 1;j<=n; j++){
                int x = i*j;
                str += x;
            }
            str+="\n";
        }

        System.out.println(str);
    }
}
