package HW3;

import java.util.Scanner;

public class Hw6 {
    public static void main(String[] args){
        draw14();
    }

    static void draw14(){

        Scanner input = new Scanner(System.in);
        System.out.print("Input n: ");
        int n = input.nextInt();
        input.close();

        String a = "*";
        String b = "-";

        String str = "";

        for(int i = 1;i<=n;i++){

            for(int j = 1; j<=n;j++){
                if(i>=j){
                    str += a;
                }
                else{
                    str += b;
                }
            }
            str += "\n";

        }

        System.out.println(str);
    }
}
