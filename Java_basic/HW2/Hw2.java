package HW2;

public class Hw2 {
    
    public static void main (String[] args){

        String[][] table = {
            {"1","2","3"},
            {"4","5","6"},
            {"7","8","9"}
        };

        String str = "";

        for(String[] arr: table){
            for(String j: arr){
                int num = Integer.parseInt(j);
                int num2 = num*2;
                str+= num2+", ";
            }
        }

        System.out.println(str);
    }
}
