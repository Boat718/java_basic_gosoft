package HW4;

import java.util.Scanner;

public class Hw4 {
    public static void main (String[] args){

        Scanner input = new Scanner(System.in);
        System.out.print("Input n: ");
        int n = input.nextInt();
        input.close();

        for(int i = 1; i<=12; i++){
            int m = i*n;
            String str = n+" x "+i+" = "+m;
            System.out.println(str);
        }
    }
}
