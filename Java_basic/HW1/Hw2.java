package HW1;

import java.util.Scanner;

public class Hw2 {
    
    public static void main(String[] args) {
		
		draw2();
	}

    public static void draw2() {
		@SuppressWarnings("resource")
		Scanner input = new Scanner (System.in);
		System.out.print("Input num: ");
		int x = input.nextInt();
		
		for(int i = 1; i <= x; i++) {
			String p = "*";
			for(int j = 1; j<x; j++) {
				p+="*";
			}
			System.out.println(p);
		}
	}
}
