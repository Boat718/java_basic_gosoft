package HW1;

import java.util.Scanner;

public class Hw1 {
    public static void main(String[] args) {
		
		draw1();
	}

	@SuppressWarnings("resource")
	public static void draw1() {
		Scanner input = new Scanner (System.in);
		System.out.print("Input num: ");
		int x = input.nextInt();
		
		String p = "*";
		for(int i = 1; i < x; i++) {
			p+="*";
		}
		System.out.println(p);
	}
}
