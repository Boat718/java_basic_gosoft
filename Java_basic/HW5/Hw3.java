package HW5;

import java.util.Scanner;

public class Hw3 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.print("Input n: ");
        int n = input.nextInt();
        input.close();

        String str = "";

        for(int i = 1; i<=(n*2)-1; i++){
            
            if(i<=n){
                
                for(int j = n;j>0;j--){
                    if(i>=j){
                        str+="*";
                    }
                    else{
                        str+="-";
                    }
                }

            }else{
                for(int j = 1;j<=n;j++){
                    if((i-j)<n){
                        str+="*";
                    }
                    else{
                        str+="-";
                    }
                }

            }
            str+="\n";
        }
        System.out.println(str);
    }
}
