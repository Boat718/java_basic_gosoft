package HW5;

import java.util.Scanner;

public class Hw6 {

    public static void main(String[] args){
    Scanner input = new Scanner(System.in);
        System.out.print("Input n: ");
        int n = input.nextInt();
        input.close();

        String str ="";

        for(int i =n; i >0 ; i--){

            for(int j = 1 ; j<=(n*2)-1; j++){
                if(j>(n-i) && j<(n+i)){
                    str+="*";
                }else{
                    str += "-";
                }

            }
            str += "\n";
        }
        System.out.println(str);
    }
}
