package HW5;

import java.util.Scanner;

public class Hw2 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.print("Input n: ");
        int n = input.nextInt();
        input.close();

        String str = "";

        for(int i = 1; i<=n; i++){

            for(int j = 1;j<=n;j++){
                if(i<=j){
                    str+="*";
                }
                else{
                    str+="-";
                }
            }
            str+="\n";
        }
        System.out.println(str);
    }
}
